# Stage 1: Build the application
FROM maven:3.8.4-openjdk-17
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean install

# Stage 2: Run the application
FROM openjdk:17-alpine
WORKDIR /app
COPY target/cnm-back-end-0.0.1-SNAPSHOT.jar ./cnm-back-end.jar
EXPOSE 8080
CMD ["java", "-jar", "cnm-back-end.jar"]
