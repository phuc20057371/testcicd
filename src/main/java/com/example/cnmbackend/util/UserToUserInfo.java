package com.example.cnmbackend.util;

import com.example.cnmbackend.dtos.UserInfo;
import com.example.cnmbackend.entities.User;

public class UserToUserInfo {
    public UserInfo convertToUserInfo(User user) {
        return UserInfo.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .avatar(user.getAvatar())
                .phoneNumber(user.getPhoneNumber())
                .dateOfBirth(user.getDateOfBirth())
                .gender(user.getGender())
                .coverImage(user.getCoverImage())
                .build();
    }
}
