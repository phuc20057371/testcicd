package com.example.cnmbackend;

import com.example.cnmbackend.repositories.AddFriendRequestRepository;
import com.example.cnmbackend.repositories.UserRepository;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title = "CNM Back End",
				version = "1.0",
				description = "This is a sample Spring Boot RESTful service using springdoc-openapi and OpenAPI 3."
		),
		servers = {
				@Server(url = "http://cnmzalo.ddns.net:8080/swagger-ui/index.html", description = "Production server"),
		}
)
public class CnmBackEndApplication {


	public static void main(String[] args) {
		SpringApplication.run(CnmBackEndApplication.class, args);
	}

//	@Bean
	CommandLineRunner runner(UserRepository userRepository, AddFriendRequestRepository addFriendRequestRepository, PasswordEncoder passwordEncoder) {
		return args -> {
			System.out.println(passwordEncoder.encode("123456"));
		};
	}

}
