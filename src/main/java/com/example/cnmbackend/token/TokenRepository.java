package com.example.cnmbackend.token;

import com.example.cnmbackend.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface TokenRepository extends MongoRepository<Token, String> {
    List<Token> findAllByUserAndExpiredIsFalseOrRevokedIsFalse(User user);

    Optional<Token> findByToken(String token);
}
