package com.example.cnmbackend.enums;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
