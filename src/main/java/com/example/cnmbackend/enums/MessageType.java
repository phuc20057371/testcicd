package com.example.cnmbackend.enums;

public enum MessageType {
    TEXT,
    IMAGE,
    VIDEO,
    FILE,
    AUDIO
}
