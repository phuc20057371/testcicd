package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TextMessage extends Message{
    private String text;

    public TextMessage(MessageType type, User sender, LocalDateTime createdAt, String text) {
        super(type, sender, createdAt);
        this.text = text;
    }
}
