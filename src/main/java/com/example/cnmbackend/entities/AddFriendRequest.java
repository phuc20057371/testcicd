package com.example.cnmbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

@Document(collection = "add-friend-requests")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddFriendRequest {
    @Id
    private String id;
    @DocumentReference
    private User sender;
    @DocumentReference
    private User receiver;

    public AddFriendRequest(User sender, User receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }
}
