package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageMessage extends Message {
    private String text;
    private String imageUrl;

    public ImageMessage(MessageType type, User sender, LocalDateTime createdAt, String text, String imageUrl) {
        super(type, sender, createdAt);
        this.text = text;
        this.imageUrl = imageUrl;
    }
}
