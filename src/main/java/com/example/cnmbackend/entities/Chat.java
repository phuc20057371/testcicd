package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.ChatType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("chats")
public class Chat {
    private String id;
    private ChatType type;
    @DocumentReference
    private List<User> members;
    private List<Message> messages;

    public Chat(String id) {
        this.id = id;
    }

    public Chat(ChatType type, List<User> members, List<Message> messages) {
        this.type = type;
        this.members = members;
        this.messages = messages;
    }
}
