package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @Id
    private String id;
    private MessageType type;
    @DocumentReference
    private User sender;
    private LocalDateTime createdAt;

    public Message(MessageType type, User sender, LocalDateTime createdAt) {
        this.type = type;
        this.sender = sender;
        this.createdAt = createdAt;
    }
}
