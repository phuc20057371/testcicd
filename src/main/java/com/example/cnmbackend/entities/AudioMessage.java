package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AudioMessage extends Message{
    private String audioUrl;
    public AudioMessage(MessageType type, User sender, LocalDateTime createdAt, String audioUrl) {
        super(type, sender, createdAt);
        this.audioUrl = audioUrl;
    }
}
