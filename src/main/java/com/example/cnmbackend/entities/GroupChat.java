package com.example.cnmbackend.entities;

import com.example.cnmbackend.enums.ChatType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupChat extends Chat{
    private String name;
    private String avatarUrl;
    @DocumentReference
    private User owner;

    public GroupChat(ChatType type, List<User> members, List<Message> messages, String name, String avatarUrl, User owner) {
        super(type, members, messages);
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.owner = owner;
    }
}
