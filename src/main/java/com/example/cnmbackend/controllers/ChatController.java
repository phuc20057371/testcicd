package com.example.cnmbackend.controllers;

import com.example.cnmbackend.dtos.CreateChatRequest;
import com.example.cnmbackend.dtos.MessageResponse;
import com.example.cnmbackend.dtos.SendMessageRequest;
import com.example.cnmbackend.entities.*;
import com.example.cnmbackend.enums.MessageType;
import com.example.cnmbackend.services.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/chats")
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;

    private final SimpMessagingTemplate simpMessagingTemplate;

    @PostMapping("/create")
    public Chat createChat(@RequestBody CreateChatRequest request) {
        return chatService.createChat(request);
    }

    @PostMapping("/update")
    public Chat updateChat(Chat chat) {
        return chatService.updateChat(chat);
    }

    @PostMapping("/delete")
    public void deleteChat(String chatId) {
        chatService.deleteChatById(chatId);
    }

    @PostMapping("/send-message")
    public ResponseEntity<?> saveMessage(@RequestBody SendMessageRequest request) {
        MessageResponse response = chatService.saveMessage(request);
        simpMessagingTemplate.convertAndSendToUser(request.getChatId(), "/topic/messages", response);
        return ResponseEntity.ok("OK");
    }

}
