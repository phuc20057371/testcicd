package com.example.cnmbackend.controllers;

import com.example.cnmbackend.configs.LogoutService;
import com.example.cnmbackend.dtos.AuthenticationRequest;
import com.example.cnmbackend.dtos.AuthenticationResponse;
import com.example.cnmbackend.dtos.RegisterRequest;
import com.example.cnmbackend.services.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Tag(name = "Authentication", description = "To perform authentication")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    private final LogoutHandler logoutHandler;


    @Operation(
            summary = "Authenticate user",
            description = "Authenticate user with phone number (10-11 Number) and password"

    )
    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    @Operation(
            summary = "Register user",
            description = "Phone number must be 10-11 Number and not present in the database, User must at least 12 years old"
    )

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
//        AuthenticationResponse response = authenticationService.register(request);
//        if(response == null) {
//            return ResponseEntity.badRequest().body("Some thing went wrong");
//        }
//        return ResponseEntity.ok(response);
        return authenticationService.register(request);
    }
    @Operation(
            summary = "Refresh token",
            description = "In case of token expired, use this endpoint to get new token"
    )
    @PostMapping("/refresh-token")
    public ResponseEntity<AuthenticationResponse> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return ResponseEntity.ok(authenticationService.refreshToken(request,response));
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        SecurityContextHolder.clearContext();
        logoutHandler.logout(request,response, authentication);
        return ResponseEntity.ok("Logout success");
    }
}
