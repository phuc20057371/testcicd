package com.example.cnmbackend.controllers;

import com.example.cnmbackend.dtos.AddFriendReq;
import com.example.cnmbackend.dtos.FriendResponse;
import com.example.cnmbackend.dtos.UserResponse;
import com.example.cnmbackend.entities.AddFriendRequest;
import com.example.cnmbackend.entities.User;
import com.example.cnmbackend.repositories.AddFriendRequestRepository;
import com.example.cnmbackend.repositories.UserRepository;
import com.example.cnmbackend.services.AddFriendService;
import com.example.cnmbackend.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Tag(name = "User", description = "To perform user actions")
public class UserController {

    private final UserService userService;
    private final AddFriendService addFriendService;

    @Operation(
            summary = "add user to black list",
            description = "need phone number of user to add to black list"

    )
    @PostMapping("/black-list")
    public ResponseEntity<String> blackList(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getCurrentUser(authentication);
        String result = userService.blackList(user, req);
        if (result.equals("OK")) {
            return ResponseEntity.ok("update black list success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "remove user from black list",
            description = "need phone number of user to remove from black list"

    )
    @PostMapping("/un-black-list")
    public ResponseEntity<String> unBlackList(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getCurrentUser(authentication);
        String result = userService.unBlackList(user, req);
        if (result.equals("OK")) {
            return ResponseEntity.ok("update black list success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "send friend request",
            description = "need phone number of user to send friend request"

    )
    @PostMapping("/add-friend-request")
    public ResponseEntity<String> addFriendRequest(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userAuthen = (User) authentication.getPrincipal();
//        User user = userRepository.findUserByPhoneNumber(userAuthen.getPhoneNumber()).orElseThrow();
//        User userFriend = userRepository.findUserByPhoneNumber(req.getPhoneNumber()).orElseThrow();
        User user = userService.getCurrentUser(authentication);
        User userFriend = userService.findUserByPhoneNumber(req.getPhoneNumber());
        String result = addFriendService.addFriendRequest(user, userFriend);
        if (result.equals("OK")) {
            return ResponseEntity.ok("add friend request success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }

    }
    @Operation(
            summary = "accept friend request",
            description = "need phone number of user to accept friend request"

    )
    @PostMapping("/accept-friend-request")
    public ResponseEntity<String> acceptFriendRequest(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userAuthen = (User) authentication.getPrincipal();
//        User user = userRepository.findUserByPhoneNumber(userAuthen.getPhoneNumber()).orElseThrow();
//        User userFriend = userRepository.findUserByPhoneNumber(req.getPhoneNumber()).orElseThrow();
        User user = userService.getCurrentUser(authentication);
        User userFriend = userService.findUserByPhoneNumber(req.getPhoneNumber());
        String result = addFriendService.acceptFriendRequest(user, userFriend);

        if (result.equals("OK")) {
            return ResponseEntity.ok("accept friend request success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "delete friend request",
            description = "need phone number of user to delete friend request"

    )
    @PostMapping("/delete-friend-request")
    public ResponseEntity<String> deleteFriendRequest(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        User user = userRepository.findUserByPhoneNumber(userAuthen.getPhoneNumber()).orElseThrow();
//        User userFriend = userRepository.findUserByPhoneNumber(req.getPhoneNumber()).orElseThrow();
        User user = userService.getCurrentUser(authentication);
        User userFriend = userService.findUserByPhoneNumber(req.getPhoneNumber());

        String result = addFriendService.deleteFriendRequest(user, userFriend);
        if (result.equals("OK")) {
            return ResponseEntity.ok("delete friend request success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "delete friend",
            description = "need phone number of user to delete friend"

    )
    @PostMapping("/delete-friend")
    public ResponseEntity<String> deleteFriend(@RequestBody AddFriendReq req) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        User user = userRepository.findUserByPhoneNumber(userAuthen.getPhoneNumber()).orElseThrow();
//        User userFriend = userRepository.findUserByPhoneNumber(req.getPhoneNumber()).orElseThrow();
        User user = userService.getCurrentUser(authentication);
        User userFriend = userService.findUserByPhoneNumber(req.getPhoneNumber());

        String result = addFriendService.deleteFriend(user, userFriend);
        if (result.equals("OK")) {
            return ResponseEntity.ok("delete friend request success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "get user info",
            description = "get info of current user"

    )
    @GetMapping("/user-info")
    public ResponseEntity<UserResponse> userInfo() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userInfo =  userService.getCurrentUser(authentication);
        if (userInfo == null) {
            return ResponseEntity.badRequest().body(null);
        } else {
            return userService.getUserInfo(userInfo);
        }
    }
    @Operation(
            summary = "search user",
            description = "search user by phone number"

    )
    @GetMapping("/find-user")
    public ResponseEntity<UserResponse> searchUser(@RequestParam AddFriendReq phoneNumber) {
        UserResponse userInfo = userService.findUser(phoneNumber.getPhoneNumber());
        if (userInfo == null) {
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.ok(userInfo);
    }

    @Operation(
            summary = "update user info",
            description = "update current user info with all field change except phone number"

    )
    @PutMapping("/update-user")
    public ResponseEntity<String> addFriendRequest(@RequestBody User newUser) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userInfo = userService.getCurrentUser(authentication);

        String result = userService.changeInfo(userInfo, newUser);
        if (result.equals("OK")) {
            return ResponseEntity.ok("update user success");
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
    @Operation(
            summary = "get friend request list",
            description = "get friend request list of current user"

    )
    @GetMapping("/get-friend-list")
    public ResponseEntity<List<String>> getFriendList() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getCurrentUser(authentication);
        List<String> friendList = null;
        try {
            friendList = user.getFriendList();
        }catch (Exception e){
            return ResponseEntity.ok(new ArrayList<>());
        }

        if (friendList == null) {
            return ResponseEntity.ok(new ArrayList<>());
        }
        return ResponseEntity.ok(friendList);
    }
    @Operation(
            summary = "get black list",
            description = "get friend black list of current user"

    )
    @GetMapping("/get-black-list")
    public ResponseEntity<List<String>> getBlacklist() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getCurrentUser(authentication);
        List<String> blackList = user.getBlackList();
        if (blackList == null) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.ok(blackList);
    }
    @Operation(
            summary = "get Friend",
            description = "get All Friend of current user (return Object User))"
    )
    @GetMapping("/get-friend")
    public ResponseEntity<FriendResponse> getFiends() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getCurrentUser(authentication);
        FriendResponse friendRequestList = userService.getFriends(user);
        if (friendRequestList == null) {
            return ResponseEntity.ok(new FriendResponse(new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        }
        return ResponseEntity.ok(friendRequestList);
    }

}
