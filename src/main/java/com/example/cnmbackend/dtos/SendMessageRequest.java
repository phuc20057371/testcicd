package com.example.cnmbackend.dtos;

import com.example.cnmbackend.entities.User;
import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendMessageRequest {
    private String chatId;
    private MessageType type;
    private LocalDateTime createdAt;
    private String senderId;
    private String text;
    private String imageUrl;
    private String audioUrl;
    private String videoUrl;
}
