package com.example.cnmbackend.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FriendResponse {
    private List<UserInfo> friendList;
    private List<UserInfo> sentFriendRequestList;
    private List<UserInfo> receivedFriendRequestList;
}
