package com.example.cnmbackend.dtos;

import com.example.cnmbackend.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageResponse {
    private String id;
    private MessageType type;
    private LocalDateTime createdAt;
    private String audioUrl;
    private String text;
    private String imageUrl;
    private String videoUrl;
    private String senderId;
    private String senderName;
    private String senderAvatar;
}
