package com.example.cnmbackend.repositories;

import com.example.cnmbackend.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findUserByPhoneNumber(String phoneNumber);

    Optional<Object> findUserById(String id);
}
