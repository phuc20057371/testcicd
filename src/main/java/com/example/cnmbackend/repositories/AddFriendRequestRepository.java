package com.example.cnmbackend.repositories;

import com.example.cnmbackend.entities.AddFriendRequest;
import com.example.cnmbackend.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface AddFriendRequestRepository extends MongoRepository<AddFriendRequest, String> {
    AddFriendRequest findBySenderAndReceiver(User sender, User receiver);



    ArrayList<User> findReceiverBySender(User userInfo);
}
