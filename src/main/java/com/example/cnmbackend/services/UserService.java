package com.example.cnmbackend.services;

import com.example.cnmbackend.dtos.AddFriendReq;
import com.example.cnmbackend.dtos.FriendResponse;
import com.example.cnmbackend.dtos.UserInfo;
import com.example.cnmbackend.dtos.UserResponse;
import com.example.cnmbackend.entities.AddFriendRequest;
import com.example.cnmbackend.entities.User;
import com.example.cnmbackend.repositories.AddFriendRequestRepository;
import com.example.cnmbackend.repositories.UserRepository;
import com.example.cnmbackend.util.UserToUserInfo;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final AddFriendRequestRepository addFriendRequestRepository;
    private final PasswordEncoder passwordEncoder;



    public User getCurrentUser(Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        return userRepository.findUserByPhoneNumber(user.getPhoneNumber()).orElse(null);

    }

    public UserResponse findUser(String phoneNumber) {
        User findUser = userRepository.findUserByPhoneNumber(phoneNumber).orElse(null);


        return UserResponse.builder()
                .id(findUser.getId())
                .phoneNumber(findUser.getPhoneNumber())
                .firstName(findUser.getFirstName())
                .lastName(findUser.getLastName())
                .avatar(findUser.getAvatar())
                .coverImage(findUser.getCoverImage())
                .gender(findUser.getGender())
                .dateOfBirth(findUser.getDateOfBirth())
                .build();


    }

    public String blackList(User user, AddFriendReq req) {
        if (user.getBlackList() == null) {
            user.setBlackList(new ArrayList<>());
            user.getBlackList().add(req.getPhoneNumber());
        } else if (user.getBlackList().contains(req.getPhoneNumber())) {
            return "User in black list";
        } else {
            user.getBlackList().add(req.getPhoneNumber());

            User temp = userRepository.findUserByPhoneNumber(req.getPhoneNumber()).orElse(null);
            AddFriendRequest friendRequestSend = addFriendRequestRepository.findBySenderAndReceiver(user, temp);
            AddFriendRequest friendRequestReceive = addFriendRequestRepository.findBySenderAndReceiver(temp, user);
            if (friendRequestSend != null) {
                addFriendRequestRepository.delete(friendRequestSend);
            }
            if (friendRequestReceive != null) {
                addFriendRequestRepository.delete(friendRequestReceive);
            }
        }
        userRepository.save(user);
        return "OK";
    }

    public String unBlackList(User user, AddFriendReq req) {
        if (user.getBlackList() == null) {
            user.setBlackList(new ArrayList<>());
        } else if (user.getBlackList().contains(req.getPhoneNumber())) {
            user.getBlackList().remove(req.getPhoneNumber());
        } else {
            return "User not in black list";
        }
        userRepository.save(user);
        return "OK";

    }

    public void save(User newUser) {
        userRepository.save(newUser);
    }

    public String updateByPhoneNumber(User userInfo, User newUser) {


        if (userInfo == null || newUser == null) {
            return "User not found";
        } else if (userInfo.getPhoneNumber().equals(newUser.getPhoneNumber())) {
            newUser.setId(userInfo.getId());
            changeInfo(userInfo, newUser);
            userRepository.save(userInfo);
            return "OK";
        } else {
            return "Cannot change the other user's information";
        }
    }

    public String changeInfo(User userInfo, User newUser) {
        if (!userInfo.getPhoneNumber().equals(newUser.getPhoneNumber())) {
            return "Cannot change the other user's information";
        }
        Class<?> obj1 = newUser.getClass();
        Field[] field1 = obj1.getDeclaredFields();

        Class<?> obj2 = newUser.getClass();
        Field[] field2 = obj2.getDeclaredFields();


        for (Field field : field1) {
            field.setAccessible(true);
            try {
                String fieldName = field.getName();
                for (Field field3 : field2) {
                    field3.setAccessible(true);
                    String fieldName2 = field3.getName();
                    if (fieldName.equals(fieldName2)) {
                        Object value = field.get(newUser);
                        if (value != null) {
                            if (fieldName.equals("password")) {
                                value = passwordEncoder.encode((String) value);
                            }
                            field.set(userInfo, value);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Error";
            }
        }
        userRepository.save(userInfo);
        return "OK";
    }


    public FriendResponse getFriends(User user) {
        User currentUser = userRepository.findUserByPhoneNumber(user.getPhoneNumber()).orElse(null);
        if (currentUser == null) {
            return null;
        }
        UserToUserInfo userToUserInfo = new UserToUserInfo();

        List<String> friendListString = currentUser.getFriendList();
        List<UserInfo> friendList = new ArrayList<>();
        for (String friend : friendListString
        ) {
            User temp = userRepository.findUserByPhoneNumber(friend).orElse(null);
            if (temp != null) {

                UserInfo t = userToUserInfo.convertToUserInfo(temp);
                friendList.add(t);
            }



        }
        List<UserInfo> sentFriendRequestList = new ArrayList<>();
        List<UserInfo> receivedFriendRequestList = new ArrayList<>();

        List<AddFriendRequest> friendListRequestUser = addFriendRequestRepository.findAll();
        for (AddFriendRequest friendRequest : friendListRequestUser) {
            if (friendRequest.getSender().getId().equals(currentUser.getId())) {

                User temp = (User) userRepository.findUserById(friendRequest.getReceiver().getId()).orElse(null);
                if (temp != null) {
                    UserInfo item = UserInfo.builder()
                            .id(temp.getId())
                            .phoneNumber(temp.getPhoneNumber())
                            .firstName(temp.getFirstName())
                            .lastName(temp.getLastName())
                            .avatar(temp.getAvatar())
                            .gender(temp.getGender())
                            .dateOfBirth(temp.getDateOfBirth())
                            .coverImage(temp.getCoverImage())
                            .build();
                    sentFriendRequestList.add(item);
                }

            }
            if (friendRequest.getReceiver().getId().equals(currentUser.getId())) {
                User temp = (User) userRepository.findUserById(friendRequest.getSender().getId()).orElse(null);
                if (temp != null) {
                    UserInfo item = UserInfo.builder()
                            .id(temp.getId())
                            .phoneNumber(temp.getPhoneNumber())
                            .firstName(temp.getFirstName())
                            .lastName(temp.getLastName())
                            .avatar(temp.getAvatar())
                            .gender(temp.getGender())
                            .dateOfBirth(temp.getDateOfBirth())
                            .coverImage(temp.getCoverImage())
                            .build();
                    receivedFriendRequestList.add(item);
                }

            }


        }


        return FriendResponse.builder()
                .friendList(friendList)
                .sentFriendRequestList(sentFriendRequestList)
                .receivedFriendRequestList(receivedFriendRequestList)
                .build();


    }



    public User findUserByPhoneNumber(String phoneNumber) {
        return userRepository.findUserByPhoneNumber(phoneNumber).orElse(null);
    }

    public ResponseEntity<UserResponse> getUserInfo(User userInfo) {
        List<AddFriendRequest> friendListRequestUser = addFriendRequestRepository.findAll();
        ArrayList<String> sentFriendList = new ArrayList<>();
        ArrayList<String> receivedFriendList = new ArrayList<>();
        for (AddFriendRequest friendRequest : friendListRequestUser) {
            if (friendRequest.getSender().getId().equals(userInfo.getId())) {
                sentFriendList.add(friendRequest.getReceiver().getPhoneNumber());
            }
            if (friendRequest.getReceiver().getId().equals(userInfo.getId())) {
                receivedFriendList.add(friendRequest.getSender().getPhoneNumber());
            }

        }


        UserResponse userResponse = UserResponse.builder()
                .id(userInfo.getId())
                .phoneNumber(userInfo.getPhoneNumber())
                .firstName(userInfo.getFirstName())
                .lastName(userInfo.getLastName())
                .avatar(userInfo.getAvatar())
                .coverImage(userInfo.getCoverImage())
                .friendList(userInfo.getFriendList())
                .blackList(userInfo.getBlackList())
                .dateOfBirth(userInfo.getDateOfBirth())
                .gender(userInfo.getGender())
                .sentFriendRequestList(sentFriendList)
                .receivedFriendRequestList(receivedFriendList)
                .build();
        return ResponseEntity.ok(userResponse);
    }
}
