package com.example.cnmbackend.services;

import com.example.cnmbackend.configs.JwtService;
import com.example.cnmbackend.dtos.AuthenticationRequest;
import com.example.cnmbackend.dtos.AuthenticationResponse;
import com.example.cnmbackend.dtos.RegisterRequest;
import com.example.cnmbackend.entities.User;
import com.example.cnmbackend.repositories.UserRepository;
import com.example.cnmbackend.token.Token;
import com.example.cnmbackend.token.TokenRepository;
import com.example.cnmbackend.token.TokenType;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final JwtService jwtService;

    private final TokenRepository tokenRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;


    public ResponseEntity<?> register(RegisterRequest request) {
        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .password(passwordEncoder.encode(request.getPassword()))
                .phoneNumber(request.getPhoneNumber())
                .dateOfBirth(request.getDateOfBirth())
                .gender(request.getGender())
                .avatar("https://res.cloudinary.com/djuwysj2y/image/upload/v1699265075/onoodmvs44ociyrnrzyq.jpg")
                .coverImage("https://res.cloudinary.com/djuwysj2y/image/upload/v1699169146/cld-sample-4.jpg")
                .build();

        if (checkUser(user).equalsIgnoreCase("OK")) {
            var savedUser = userRepository.save(user);
            var jwtToken = jwtService.generateToken(user);
            var refreshToken = jwtService.generateRefreshToken(user);
            saveUserToken(savedUser, jwtToken);
            return ResponseEntity.ok(AuthenticationResponse.builder()
                    .accessToken(jwtToken)
                    .refreshToken(refreshToken)
                    .build());
        }
        return ResponseEntity.badRequest().body(checkUser(user));
    }


    public String checkUser(User user) {

        // kiểm tra số điện thoại có đúng định dạng không
        // số điện thoại có 10 hoặc 11 số
        // số điện thoại bắt đầu bằng 0 hoặc +84
        String regex = "^(0|\\+84)\\d{9,10}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(user.getPhoneNumber());

        if (!matcher.matches()) {
            return "sai dinh dang so dien thoai";
        }

        User findUser = userRepository.findUserByPhoneNumber(user.getPhoneNumber()).orElse(null);
        if (findUser != null) {
            return "user da ton tai";
        }

        if (LocalDate.now().getYear() - user.getDateOfBirth().getYear() <= 12) {
            return "tuoi khong hop le (>12)";

        }

        return "OK";
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getPhoneNumber(),
                        request.getPassword()
                )
        );
        User user = userRepository.findUserByPhoneNumber(request.getPhoneNumber()).orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeUserToken(user);
        saveUserToken(user, jwtToken);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    private void revokeUserToken(User user) {
        var validUserTokens = tokenRepository.findAllByUserAndExpiredIsFalseOrRevokedIsFalse(user);
        if (validUserTokens.isEmpty()) {
            return;
        }
        validUserTokens.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
            tokenRepository.saveAll(validUserTokens);
        });

    }

    private void saveUserToken(User savedUser, String jwtToken) {
        var token = Token.builder()
                .user(savedUser)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    public AuthenticationResponse refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authHeader = request.getHeader("Authorization");
        AuthenticationResponse authenticationResponse = null;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return authenticationResponse;
        }
        String refreshToken = authHeader.substring(7);
        String userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null) {
            User userDetails = userRepository.findUserByPhoneNumber(userEmail).orElseThrow();

            if (jwtService.isTokenValid(refreshToken)) {
                String accessToken = jwtService.generateToken(userDetails);
                revokeUserToken(userDetails);
                saveUserToken(userDetails, accessToken);
                authenticationResponse = AuthenticationResponse.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();

            }
            new ObjectMapper().writeValue(response.getOutputStream(), authenticationResponse);
        }
        return authenticationResponse;
    }
}