package com.example.cnmbackend.services;

import com.example.cnmbackend.entities.AddFriendRequest;
import com.example.cnmbackend.entities.User;
import com.example.cnmbackend.repositories.AddFriendRequestRepository;
import com.example.cnmbackend.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class AddFriendService {
    private final AddFriendRequestRepository addFriendRepository;
    private final UserRepository userRepository;

    public AddFriendRequest findBySenderAndReceiver(User sender, User receiver) {
        return addFriendRepository.findBySenderAndReceiver(sender, receiver);
    }

    public String deleteFriendRequest(User user, User userFriend) {
        AddFriendRequest addFriendRequest = null;
        try {
           addFriendRequest = addFriendRepository.findBySenderAndReceiver(userFriend,user);

        } catch (Exception e) {
            return "NOT FOUND";
        }
        if (addFriendRequest == null) {
            return "NOT FOUND";
        } else {
            addFriendRepository.delete(addFriendRequest);
            return "OK";
        }
    }

    public String addFriendRequest(User user, User userFriend) {
        if (user.getBlackList() == null)
            user.setBlackList(new ArrayList<>());
        if (user.getFriendList() == null)
            user.setFriendList(new ArrayList<>());

        if (userFriend.getFriendList() == null)
            userFriend.setFriendList(new ArrayList<>());
        if (userFriend.getBlackList() == null)
            userFriend.setBlackList(new ArrayList<>());

        if (userFriend.getBlackList().contains(user.getPhoneNumber())) {
            return "User in black list";
        } else if (user.getPhoneNumber().equals(userFriend.getPhoneNumber())) {
            return "Can't add yourself";
        } else if (user.getFriendList().contains(userFriend.getPhoneNumber())) {
            return "Already friend";
        }

        AddFriendRequest addFriendRequest = null;
        try {
            addFriendRequest = addFriendRepository.findBySenderAndReceiver(user, userFriend);
        } catch (Exception e) {
            return "NOT FOUND";
        }
        if (addFriendRequest == null) {
            addFriendRequest = new AddFriendRequest(user, userFriend);
            addFriendRepository.save(addFriendRequest);
            return "OK";
        } else {
            return "Already sent request";
        }
    }

    public String acceptFriendRequest(User user, User userFriend) {
        AddFriendRequest addFriendRequest = null;
        try {
            addFriendRequest = addFriendRepository.findBySenderAndReceiver(user, userFriend);
        } catch (Exception e) {
            return "NOT FOUND";
        }

        if (addFriendRequest == null) {
            return "NOT FOUND";
        } else {
            if(user.getFriendList() == null)
                user.setFriendList(new ArrayList<>());
            user.getFriendList().add(userFriend.getPhoneNumber());
            userRepository.save(user);
            if(userFriend.getFriendList() == null)
                userFriend.setFriendList(new ArrayList<>());
            userFriend.getFriendList().add(user.getPhoneNumber());
            userRepository.save(userFriend);
            addFriendRepository.delete(addFriendRequest);

            return "OK";
        }
    }

    public String deleteFriend(User user, User userFriend) {
        if (user.getFriendList() == null) {
            user.setFriendList(new ArrayList<>());
        } else if (user.getFriendList().contains(userFriend.getPhoneNumber())) {
            user.getFriendList().remove(userFriend.getPhoneNumber());
        } else {
            return "User not in friend list";
        }
        userRepository.save(user);

        if (userFriend.getFriendList() == null) {
            userFriend.setFriendList(new ArrayList<>());
        } else if (userFriend.getFriendList().contains(user.getPhoneNumber())) {
            userFriend.getFriendList().remove(user.getPhoneNumber());
        } else {
            return "User not in friend list";
        }
        userRepository.save(userFriend);

        return "OK";

    }
}
