package com.example.cnmbackend.services;

import com.example.cnmbackend.dtos.CreateChatRequest;
import com.example.cnmbackend.dtos.MessageResponse;
import com.example.cnmbackend.dtos.SendMessageRequest;
import com.example.cnmbackend.entities.*;
import com.example.cnmbackend.enums.ChatType;
import com.example.cnmbackend.enums.MessageType;
import com.example.cnmbackend.repositories.ChatRepository;
import com.example.cnmbackend.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChatService {

    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    private final MongoTemplate mongoTemplate;

    public Chat createChat(CreateChatRequest request) {
        Chat chat = null;
        if (request.getMembers().size() > 2) {
            List<User> members = new ArrayList<>();
            request.getMembers().forEach(member -> {
                members.add(new User(member));
            });
            chat = new GroupChat(
                    ChatType.GROUP,
                    members,
                    new ArrayList<Message>(),
                    "Group chat",
                    "https://picsum.photos/200/300",
                    new User(request.getOwnerId())
            );
        } else if (request.getMembers().size() == 2) {
            List<User> members = new ArrayList<>();
            request.getMembers().forEach(member -> {
                members.add(new User(member));
            });
            chat = new Chat(
                    ChatType.PRIVATE,
                    members,
                    new ArrayList<Message>()
            );
        } else {
            return null;
        }
        return chatRepository.save(chat);
    }

    public Chat getChatById(String id) {
        return chatRepository.findById(id).orElse(null);
    }

    public Chat updateChat(Chat chat) {
        return chatRepository.save(chat);
    }

    public MessageResponse saveMessage(SendMessageRequest request) {
        Message message = null;
        User sender = userRepository.findById(request.getSenderId()).orElse(null);
        if (request.getType() == MessageType.TEXT) {
            message = new TextMessage(
                    MessageType.TEXT,
                    sender,
                    LocalDateTime.now(),
                    request.getText());
        }
        if (request.getType() == MessageType.IMAGE) {
            message = new ImageMessage(
                    MessageType.IMAGE,
                    sender,
                    LocalDateTime.now(),
                    request.getText(),
                    request.getImageUrl());
        }
        if (message == null)
            return null;
        if(sender == null)
            return null;

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(request.getChatId()));
        Update update = new Update();
        update.push("messages", message);
        mongoTemplate.updateFirst(query, update, Chat.class);

        return MessageResponse.builder()
                .id(message.getId())
                .createdAt(message.getCreatedAt())
                .type(message.getType())
                .text(request.getText())
                .imageUrl(request.getImageUrl())
                .audioUrl(request.getAudioUrl())
                .videoUrl(request.getVideoUrl())
                .senderId(sender.getId())
                .senderName(sender.getFirstName() + " " + sender.getLastName())
                .senderAvatar(sender.getAvatar())
                .build();
    }

    public void deleteChatById(String id) {
        chatRepository.deleteById(id);
    }

}
